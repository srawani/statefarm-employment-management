package com.statefarm.services;

import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.statefarm.database.entity.EmployeeEntity;
import com.statefarm.database.repository.EmployeeRepository;
import com.statefarm.services.models.Employee;

@RestController
@RequestMapping("/employee")
public class EmployeeController implements Controller {

	@Autowired
	private EmployeeRepository er;

	private static Function<EmployeeEntity, Employee> translate = (x) -> new Employee(x.getId(), x.getFirstName(),
			x.getMiddleName(), x.getLastName());
	private static Function<Employee, EmployeeEntity> translateToEntity = (x) -> new EmployeeEntity(x.getId(),
			x.getFirstName(), x.getMiddleName(), x.getLastName());

	@RequestMapping(method = RequestMethod.GET)
	public List<Employee> getAll() {
		return er.getAll().stream().map(translate).collect(Collectors.toList());
	}

	@RequestMapping(method = RequestMethod.POST)
	public Employee post(@RequestBody Employee employee) {
		return translate.apply(er.post(translateToEntity.apply(employee)));
	}

	@RequestMapping(value = "{id}", method = RequestMethod.PUT)
	public Employee put(@PathVariable(value = "id") long id, @RequestBody Employee employee) {
		return translate.apply(er.put(id, translateToEntity.apply(employee)));
	}

	@RequestMapping(value = "{id}", method = RequestMethod.DELETE)
	public long delete(@PathVariable(value = "id") long id) {
		return er.delete(id);
	}

	@RequestMapping(value = "{id}", method = RequestMethod.GET)
	public Employee get(@PathVariable(value = "id") long id) {
		return translate.apply(er.get(id));
	}

}
